﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Demo.Controllers
{
    public class MemberController : Controller
    {
        public IActionResult Login()
        {
            return View();
        }
        public IActionResult Register()
        {
            return View();
        }
        public IActionResult Reservedlist()
        {
            return View();
        }
        public IActionResult Reservregister1()
        {
            return View();
        }
        public IActionResult Reservregister2()
        {
            return View();
        }
        public IActionResult Reservsurvey1()
        {
            return View();
        }
        public IActionResult Reservsurvey2()
        {
            return View();
        }
        public IActionResult Attachpic()
        {
            return View();
        }
        public IActionResult AttachpicEdit()
        {
            return View();
        }
        public IActionResult Landingpage()
        {
            return View();
        }
        public IActionResult Registercalendar()
        {
            return View();
        }
        public IActionResult Queuecheck()
        {
            return View();
        }
        public IActionResult QueueRconfirm()
        {
            return View();
        }
        public IActionResult Surveycalendar1()
        {
            return View();
        }
        public IActionResult Surveycalendar2()
        {
            return View();
        }
        public IActionResult QueueSconfirm()
        {
            return View();
        }
        public IActionResult AttachSpic()
        {
            return View();
        }
        public IActionResult Userupdate()
        {
            return View();
        }
        public IActionResult Queuepic()
        {
            return View();
        }
        public IActionResult Notification()
        {
            return View();
        }
        public IActionResult QueueCard()
        {
            return View();
        }
    }
}
