﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.Models
{
    public class TransQueue
    {
        public string user_reg_id { get; set; }
        public string trans_queue_type { get; set; }
        public string landoffice_seq { get; set; }
        public string owner_type_id { get; set; }
        public string reg_type_id { get; set; }
        public string juristic_id { get; set; }
        public string juristic_name { get; set; }
        public string evd_type_id { get; set; }
        public string province_seq { get; set; }
        public string amphur_seq { get; set; }
        public string parcel_no { get; set; }
        public string land_no { get; set; }
        public string apppointment_date { get; set; }
        public string survey_date { get; set; }
        public string created_user { get; set; }
    }
}
