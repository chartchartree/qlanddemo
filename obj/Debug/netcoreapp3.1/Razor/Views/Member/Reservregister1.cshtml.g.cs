#pragma checksum "D:\DOL\eQlands\Demo\Demo\Views\Member\Reservregister1.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "999dc128b65c16a740716ee1eea1b51babd0df6f"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Member_Reservregister1), @"mvc.1.0.view", @"/Views/Member/Reservregister1.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\DOL\eQlands\Demo\Demo\Views\_ViewImports.cshtml"
using Demo;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\DOL\eQlands\Demo\Demo\Views\_ViewImports.cshtml"
using Demo.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"999dc128b65c16a740716ee1eea1b51babd0df6f", @"/Views/Member/Reservregister1.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e18407c5b9dabc62761fc6cdd8f67817f22bc556", @"/Views/_ViewImports.cshtml")]
    public class Views_Member_Reservregister1 : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/css/member.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rel", new global::Microsoft.AspNetCore.Html.HtmlString("stylesheet"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", new global::Microsoft.AspNetCore.Html.HtmlString("text/css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/select2/css/select2.min.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Member", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Reservedlist", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-primary text-light"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/select2/js/select2.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "D:\DOL\eQlands\Demo\Demo\Views\Member\Reservregister1.cshtml"
  
    ViewData["Title"] = "e-Qlands";

#line default
#line hidden
#nullable disable
            DefineSection("Styles", async() => {
                WriteLiteral("\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "999dc128b65c16a740716ee1eea1b51babd0df6f6607", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "999dc128b65c16a740716ee1eea1b51babd0df6f7872", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n");
            }
            );
            WriteLiteral(@"
<style>
    .trim-border {
        border-radius:6px;
    }
    #next-btn.active {
        background-color: #ff6a00;
    }
   

</style>

<div id=""member-bg"">

    <div class=""container"">
        <div class=""row"">
            <div class=""col-11 m-3 justify-content-center"">
                <div class=""col-12 text-center"">
                    <p class=""text-light"">ขั้นตอนที่ 1/4</p>
                </div>
                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "999dc128b65c16a740716ee1eea1b51babd0df6f9562", async() => {
                WriteLiteral(@"
                    <div class=""bg-light p-2 mb-2 trim-border"">
                        <label class=""my-1 mr-2"" for=""inlineFormCustomSelectPref"">จังหวัด</label>
                        <select class=""custom-select my-1 mr-sm-2 validate-input"" id=""select-province"" name=""province_seq"" >
                            ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "999dc128b65c16a740716ee1eea1b51babd0df6f10149", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral(@"
                        </select>
                    </div>
                    <div class="" bg-light p-2 mb-2 trim-border"">
                        <label class=""my-1 mr-2"" for=""inlineFormCustomSelectPref"">สำนักงานที่ดิน</label>
                        <select class=""custom-select my-1 mr-sm-2 validate-input"" id=""select-office"" name=""landoffice_seq"">
                            ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "999dc128b65c16a740716ee1eea1b51babd0df6f11518", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral(@"
                        </select>
                    </div>
                    <div class=""bg-light p-2 mb-2 justify-content-center trim-border"">
                        <div class=""form-check form-check-inline "">
                            <input class=""form-check-input validate-input"" type=""radio"" name=""owner_type_id"" id=""owner_type_id1"" value=""1"" checked>
                            <label class=""form-check-label"" for=""owner_type_id1"">บุคคลธรรมดา</label>
                        </div>
                        <div class=""form-check form-check-inline "">
                            <input class=""form-check-input validate-input"" type=""radio"" name=""owner_type_id"" id=""owner_type_id2"" value=""2"">
                            <label class=""form-check-label"" for=""owner_type_id2"">นิติบุคคล</label>
                        </div>
                    </div>
                    <div class=""bg-light p-2 mb-2 justify-content-center d-none trim-border"" id=""owner_input"">
                        <div class=""f");
                WriteLiteral(@"orm-group"">
                            <input class=""form-control validate-input"" type=""text"" name=""owner_name"" placeholder=""ชื่อนิติบุคคล"">
                        </div>
                        <div class=""form-group"">
                            <input class=""form-control validate-input"" type=""text"" name=""owner_number"" placeholder=""เลขนิติบุคคล"">
                        </div>
                    </div>

                    <div class=""form-group col bg-light trim-border"" id=""reserve_type"">
                        <legend class=""col-form-label mb-2"">ประเภทคำขอจดทะเบียน</legend>


");
                WriteLiteral(@"                    </div>
                    <div class=""row justify-content-between"">

                        <div class=""col-5"">

                            <a class=""btn btn-block btn-danger text-light"" data-toggle=""modal"" data-target=""#backModal""><i class=""fas fa-times""></i>  ยกเลิก</a>
                        </div>

");
                WriteLiteral(@"                        <div class=""col-5"">

                            <a class=""btn btn-block btn-secondary text-light"" id=""next-btn"">ถัดไป  <i class=""fas fa-arrow-right""></i></a>
                        </div>
                    </div>
                ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n\r\n    <!-- Modal -->\r\n");
            WriteLiteral(@"    <div class=""modal fade"" id=""nextModal"" tabindex=""-1"" role=""dialog"" aria-labelledby=""exampleModalLabel"" aria-hidden=""true"">
        <div class=""modal-dialog modal-dialog-centered"" role=""document"">
            <div class=""modal-content"">
                <div class=""modal-header"">
                    <h5 class=""modal-title"" id=""exampleModalLabel"">ประเภทเอกสารหลักฐาน</h5>
                    <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"">
                        <span aria-hidden=""true"">&times;</span>
                    </button>
                </div>
                <div class=""modal-body"">
                    <select id=""evd_type_id"" class=""form-control"" name=""evd_type_id"">
                        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "999dc128b65c16a740716ee1eea1b51babd0df6f16784", async() => {
                WriteLiteral("เลือกประเภทเอกสารหลักฐาน");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            BeginWriteTagHelperAttribute();
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __tagHelperExecutionContext.AddHtmlAttribute("disabled", Html.Raw(__tagHelperStringValueBuffer), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.Minimized);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            WriteLiteral(@"                    </select>
                </div>
                <div class=""modal-footer"">
                    <button type=""button"" class=""btn btn-secondary"" data-dismiss=""modal"">ปิดหน้าต่าง</button>
                    <button class=""btn btn-primary"" id=""store-data-btn"">ตกลง</button>
                </div>
            </div>
        </div>
    </div>

");
            WriteLiteral(@"    <div class=""modal fade"" id=""backModal"" tabindex=""-1"" role=""dialog"" aria-labelledby=""exampleModalLabel"" aria-hidden=""true"">
        <div class=""modal-dialog modal-dialog-centered"" role=""document"">
            <div class=""modal-content"">
                <div class=""modal-body"">
                    <div class=""row justify-content-center mb-4"">
                        <i class=""far fa-question-circle fa-7x"" style=""  color: #26d1d1;""></i>
                    </div>

                    <div class=""row justify-content-center "">
                        <p>ยืนยันยกเลิกการจองคิวจดทะเบียน</p>
                    </div>
                </div>
                <div class=""modal-footer"">
                    <button type=""button"" class=""btn btn-secondary"" data-dismiss=""modal"">ปิดหน้าต่าง</button>
                    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "999dc128b65c16a740716ee1eea1b51babd0df6f19386", async() => {
                WriteLiteral("ยืนยัน");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n</div>\r\n");
            DefineSection("Scripts", async() => {
                WriteLiteral("\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "999dc128b65c16a740716ee1eea1b51babd0df6f21023", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral(@"
    <script>
        $(function () {
            $('input[name=""owner_type_id""]').change(function () {
                let ownValue = $(this).val();
                let ownerInputEl = $('#owner_input');
                if (ownValue == 2) {
                    ownerInputEl.removeClass('d-none');
                } else {
                    ownerInputEl.addClass('d-none');
                }
            });

            let provinceUrl = 'https://eqlands.dol.go.th/eQLands/api/Service/GetProvinceList';
            let provinces;
            $.ajax({
                url: provinceUrl,
                dataType: ""json"",
            }).done(function (data) {
                provinces = data.records;
                const map1 = provinces.map(function(x){
                    x.id = x.Value;
                    x.text = x.Label
                    return x;
                });
                console.log(""provinces"", map1);
                $('#select-province').select2({
                    p");
                WriteLiteral(@"laceholder: ""เลือกจังหวัด"",
                    allowClear: true,
                    data: map1
                });
                $('#select-office').select2({
                    placeholder: ""เลือกสำนักงานที่ดิน""
                });
                $('#select-province').on('select2:select', function (e) {
                    const data = {
                        province_seq: e.params.data.Value
                    }
                    let officeUrl = 'https://eqlands.dol.go.th/eQLands/api/Service/GetLandOfficeList';
                    $.ajax({
                        url: officeUrl,
                        dataType: ""json"",
                        data: data
                    }).done(function (data) {
                        let offices = data.records;
                        const officeList = offices.map(function (x) {
                            x.id = x.Value;
                            x.text = x.Label
                            return x;
                        });
  ");
                WriteLiteral(@"                      console.log('offices', officeList);
                        let officeEl = $('#select-office');
                        officeEl.empty();
                        officeEl.select2({
                            placeholder: ""เลือกสำนักงานที่ดิน"",
                            allowClear: true,
                            data: officeList
                        });
                    });
                });

                let isValid = false;



                function validatorInput() {

                    const regtypeId = $('input[name=""regtype_id""]:checked').val();
                    //const selectOffice = $('#select-office').val();
                    
                    const selectedProvicne = $('#select-province').val();
                  
                   
                    if (selectedProvicne && regtypeId   ) {
                        isValid = true;
                        $('#next-btn').addClass('active');
                        // change bt");
                WriteLiteral(@"n to available
                    }
                    else {
                        isValid = false;
                        $('#next-btn').removeClass('active');
                    }
                }
                $('.validate-input').change(function () {
                    validatorInput();
                });
                $('#next-btn').click(function () {
                    if (isValid) {
                        $('#nextModal').modal('toggle');
                    }
                    else {
                        // alert
                        alert(""ท่านกรอกข้อมูลไม่ครบ"");
                    }

                })

                $('#store-data-btn').click(function () {
                    const regtypeId = $('input[name=""regtype_id""]:checked').val();
                    const selectOffice = $('#select-office').val();
                    const ownerType = $('input[name=""owner_type_id""]:checked').val();
                    const selectedProvicne = $('#select-pro");
                WriteLiteral(@"vince').val();
                    const evdType = $('#evd_type_id option:selected').val();
                    const userInfoModel = JSON.parse(localStorage.getItem(""userinfo""));
                    console.log('infomodel', userInfoModel);
                    if (regtypeId && selectOffice && ownerType && selectedProvicne && evdType) {
                        let model = {
                            user_reg_id: userInfoModel.user_reg_id,
                            trans_queue_type: 'R',
                            landoffice_seq: selectOffice,
                            owner_type_id: ownerType,
                            reg_type_id: regtypeId,
                            juristic_id: ownerType == 2 ? $('input[name=""owner_number""]').val() : """",
                            juristic_name: ownerType == 2 ? $('input[name=""owner_name""]').val() : """",
                            province_seq: selectedProvicne,
                            evd_type_id: evdType,
                            created_");
                WriteLiteral(@"user: userInfoModel.user_citizen_id,
                        };
                        console.log('regid', model);
           
                        localStorage.setItem(""transQueue"", JSON.stringify(model));
                        localStorage.setItem(""evd_label"", $('#evd_type_id option:selected').text());
                        localStorage.setItem(""province_label"", $('#select-province option:selected').text());
                        localStorage.setItem(""landoffice_label"", $('#select-office option:selected').text());
                        //if (ownerType == 1) { localStorage.setItem(""owntype_label"", ""บุคคลธรรมดา"") }
                        //else { localStorage.setItem(""owntype_label"", ""นิติบุคคล"") }
                        ownerType == 1 ? localStorage.setItem(""owntype_label"", ""บุคคลธรรมดา"") :
                            localStorage.setItem(""owntype_label"", ""นิติบุคคล"");
                        
                        //let prot = window.location.protocol
                        /");
                WriteLiteral("/let rootPath = window.location.host;\r\n                        //let nextUrl = prot + \"//\" + rootPath + \"/Member/Reservregister2\";\r\n                        let nextUrl = \"");
#nullable restore
#line 281 "D:\DOL\eQlands\Demo\Demo\Views\Member\Reservregister1.cshtml"
                                  Write(Url.Action("Reservregister2", "Member"));

#line default
#line hidden
#nullable disable
                WriteLiteral(@""";
                        window.location.href = nextUrl;
                    } else {
                        // alert
                        alert(""ท่านกรอกข้อมูลไม่ครบ"");
                    }

                });
            });

            //2. get type
            let typeUrl = 'https://eqlands.dol.go.th/eQLands/api/Service/GetRegTypeList?reg_type_flags=R';
            let records;
            $.ajax({
                url: typeUrl,
                dataType: ""json"",
            }).done(function (data) {
                records = data.records;
                if (records) {
                    localStorage.setItem(""regTypeRecord"", JSON.stringify(records));
                }
                
                $.each(records, function (key, value) {
                    $('#reserve_type').append(
                        `<div class=""form-check"">
                            <input class=""form-check-input validate-input"" name=""regtype_id"" type=""radio"" id=""${value.Value}"" value=""${valu");
                WriteLiteral(@"e.Value}"">
                            <label class=""form-check-label mb-3"" for=""${value.Value}"">${value.Label}</label>
                        </div>`
                    )
                });

            });

            //3. get main doc
            let evdUrl = 'https://eqlands.dol.go.th/eQLands/api/Service/GetEvdTypeList?evd_type_flags=';
            $.ajax({
                url: evdUrl,
                dataType: ""json"",
            }).done(function (data) {
                $.each(data.records, function (key, value) {
                    $('#evd_type_id').append(
                        ` <option value=""${value.Value}"">${value.Label}</option>`
                    )
                });
            });


        });
    </script>
");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
