#pragma checksum "D:\DOL\eQlands\Demo\Demo\Views\Member\Attachpic.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2b0c635033d9b368145cd8b2d9eee780cc7bcfab"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Member_Attachpic), @"mvc.1.0.view", @"/Views/Member/Attachpic.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\DOL\eQlands\Demo\Demo\Views\_ViewImports.cshtml"
using Demo;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\DOL\eQlands\Demo\Demo\Views\_ViewImports.cshtml"
using Demo.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2b0c635033d9b368145cd8b2d9eee780cc7bcfab", @"/Views/Member/Attachpic.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e18407c5b9dabc62761fc6cdd8f67817f22bc556", @"/Views/_ViewImports.cshtml")]
    public class Views_Member_Attachpic : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "D:\DOL\eQlands\Demo\Demo\Views\Member\Attachpic.cshtml"
  
    ViewData["Title"] = "e-Qlands";

#line default
#line hidden
#nullable disable
            DefineSection("Styles", async() => {
                WriteLiteral(@"
    <link href=""/css/member.css"" rel=""stylesheet"" type=""text/css"" />
    <link href=""/lib/select2/css/select2.min.css"" rel=""stylesheet"" />
    <style>
        .attach-header{
            color: white;
        }

        .upload-box-header {
            border-left: solid 4px #6f7a8e;
            background-color: #f3f3f3;
        }
        .gallery-upload{
            color: salmon;
        }
        .camera-upload {
            text-align: center;
            background-color: #a7a7a7;
            color: white;
            height: 100px;
            overflow: hidden;
            border-radius: 6px;
        }
        .camera-upload i.fa-camera {
            margin: 20px;
            cursor: pointer;
            font-size: 60px;
        }

        i:hover {
            opacity: 0.6;
        }

        input {
            display: none;
        }
    </style>
");
            }
            );
            WriteLiteral(@"

<div id=""member-bg"">
    <div class=""row m-3 pt-2"">
        <div class=""col-12 text-center"">
            <div class=""d-flex attach-header"">
                <div class=""p-2"">
                    <i class=""fas fa-arrow-left""></i>
                </div>
                <div class=""p-2 flex-grow-1"" id=""attach-header""></div>
            </div>
        </div>
    </div>
    <div class=""container pt-3 pb-3"" style=""background-color:#fff"">
        <div class=""row m-2"">
            <div class=""col-12"">
");
            WriteLiteral(@"                <div class=""row p-2 mb-2 upload-box-header"">
                    <div class=""col-5"">
                        <div>รูปที่ 1</div>
                    </div>
                    <div class=""col-7"">
                        <div class=""d-flex justify-content-end gallery-upload"">
                            <div>
                                <i class=""fa fa-upload"" id=""gal1""></i>
                                <input type=""file"" id=""gallery1"" accept=""image/*"">
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""row mb-2"">
                    <div class=""col-4 camera-upload"">
                        <i class=""fa fa-camera"" id=""cam1""></i>
                        <input type=""file"" id=""camera1"" accept=""image/*"" capture />
                        <img id=""preview1"" src=""#"" style=""width:100%"" onerror=""this.style.display='none'"" />
                    </div>
                    <div class=""c");
            WriteLiteral("ol-8\">\r\n                        <textarea rows=\"4\" class=\"form-control\" placeholder=\"ใส่ข้อมูลรายละเอียดรูปภาพ\"></textarea>\r\n                    </div>\r\n                </div>\r\n");
            WriteLiteral(@"                <div class=""row p-2 mb-2 upload-box-header"">
                    <div class=""col-5"">
                        <div>รูปที่ 2</div>
                    </div>
                    <div class=""col-7"">
                        <div class=""d-flex justify-content-end gallery-upload"">
                            <div>
                                <i class=""fa fa-upload"" id=""gal2""></i>
                                <input type=""file"" id=""gallery2"" accept=""image/*"">
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""row mb-2"">
                    <div class=""col-4 camera-upload"">
                        <i class=""fa fa-camera"" id=""cam2""></i>
                        <input type=""file"" id=""camera2"" accept=""image/*"" capture />
                        <img id=""preview2"" src=""#"" style=""width:100%"" onerror=""this.style.display='none'"" />
                    </div>
                    <div class=""c");
            WriteLiteral("ol-8\">\r\n                        <textarea rows=\"4\" class=\"form-control\" placeholder=\"ใส่ข้อมูลรายละเอียดรูปภาพ\"></textarea>\r\n                    </div>\r\n                </div>\r\n");
            WriteLiteral(@"                <div class=""row p-2 mb-2 upload-box-header"">
                    <div class=""col-5"">
                        <div>รูปที่ 3</div>
                    </div>
                    <div class=""col-7"">
                        <div class=""d-flex justify-content-end gallery-upload"">
                            <div>
                                <i class=""fa fa-upload"" id=""gal3""></i>
                                <input type=""file"" id=""gallery3"" accept=""image/*"">
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""row mb-2"">
                    <div class=""col-4 camera-upload"">
                        <i class=""fa fa-camera"" id=""cam3""></i>
                        <input type=""file"" id=""camera3"" accept=""image/*"" capture />
                        <img id=""preview3"" src=""#"" style=""width:100%"" onerror=""this.style.display='none'"" />
                    </div>
                    <div class=""c");
            WriteLiteral("ol-8\">\r\n                        <textarea rows=\"4\" class=\"form-control\" placeholder=\"ใส่ข้อมูลรายละเอียดรูปภาพ\"></textarea>\r\n                    </div>\r\n                </div>\r\n\r\n");
            WriteLiteral(@"                <div class=""row p-2 mb-2 upload-box-header"">
                    <div class=""col-5"">
                        <div>รูปที่ 4</div>
                    </div>
                    <div class=""col-7"">
                        <div class=""d-flex justify-content-end gallery-upload"">
                            <div>
                                <i class=""fa fa-upload"" id=""gal4""></i>
                                <input type=""file"" id=""gallery4"" accept=""image/*"">
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""row mb-2"">
                    <div class=""col-4 camera-upload"">
                        <i class=""fa fa-camera"" id=""cam4""></i>
                        <input type=""file"" id=""camera4"" accept=""image/*"" capture />
                        <img id=""preview4"" src=""#"" style=""width:100%"" onerror=""this.style.display='none'"" />
                    </div>
                    <div class=""c");
            WriteLiteral("ol-8\">\r\n                        <textarea rows=\"4\" class=\"form-control\" placeholder=\"ใส่ข้อมูลรายละเอียดรูปภาพ\"></textarea>\r\n                    </div>\r\n                </div>\r\n\r\n");
            WriteLiteral(@"                <div class=""row p-2 mb-2 upload-box-header"">
                    <div class=""col-5"">
                        <div>รูปที่ 5</div>
                    </div>
                    <div class=""col-7"">
                        <div class=""d-flex justify-content-end gallery-upload"">
                            <div>
                                <i class=""fa fa-upload"" id=""gal5""></i>
                                <input type=""file"" id=""gallery5"" accept=""image/*"">
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""row mb-2"">
                    <div class=""col-4 camera-upload"">
                        <i class=""fa fa-camera"" id=""cam5""></i>
                        <input type=""file"" id=""camera5"" accept=""image/*"" capture />
                        <img id=""preview5"" src=""#"" style=""width:100%"" onerror=""this.style.display='none'"" />
                    </div>
                    <div class=""c");
            WriteLiteral("ol-8\">\r\n                        <textarea rows=\"4\" class=\"form-control\" placeholder=\"ใส่ข้อมูลรายละเอียดรูปภาพ\"></textarea>\r\n                    </div>\r\n                </div>\r\n\r\n");
            WriteLiteral(@"                <div class=""row p-2 mb-2 upload-box-header"">
                    <div class=""col-5"">
                        <div>รูปที่ 6</div>
                    </div>
                    <div class=""col-7"">
                        <div class=""d-flex justify-content-end gallery-upload"">
                            <div>
                                <i class=""fa fa-upload"" id=""gal6""></i>
                                <input type=""file"" id=""gallery6"" accept=""image/*"">
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""row mb-2"">
                    <div class=""col-4 camera-upload"">
                        <i class=""fa fa-camera"" id=""cam6""></i>
                        <input type=""file"" id=""camera6"" accept=""image/*"" capture />
                        <img id=""preview6"" src=""#"" style=""width:100%"" onerror=""this.style.display='none'"" />
                    </div>
                    <div class=""c");
            WriteLiteral("ol-8\">\r\n                        <textarea rows=\"4\" class=\"form-control\" placeholder=\"ใส่ข้อมูลรายละเอียดรูปภาพ\"></textarea>\r\n                    </div>\r\n                </div>\r\n\r\n");
            WriteLiteral(@"                <div class=""row p-2 mb-2 upload-box-header"">
                    <div class=""col-5"">
                        <div>รูปที่ 7</div>
                    </div>
                    <div class=""col-7"">
                        <div class=""d-flex justify-content-end gallery-upload"">
                            <div>
                                <i class=""fa fa-upload"" id=""gal7""></i>
                                <input type=""file"" id=""gallery7"" accept=""image/*"">
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""row mb-2"">
                    <div class=""col-4 camera-upload"">
                        <i class=""fa fa-camera"" id=""cam7""></i>
                        <input type=""file"" id=""camera7"" accept=""image/*"" capture />
                        <img id=""preview7"" src=""#"" style=""width:100%"" onerror=""this.style.display='none'"" />
                    </div>
                    <div class=""c");
            WriteLiteral("ol-8\">\r\n                        <textarea rows=\"4\" class=\"form-control\" placeholder=\"ใส่ข้อมูลรายละเอียดรูปภาพ\"></textarea>\r\n                    </div>\r\n                </div>\r\n\r\n");
            WriteLiteral(@"                <div class=""row p-2 mb-2 upload-box-header"">
                    <div class=""col-5"">
                        <div>รูปที่ 8</div>
                    </div>
                    <div class=""col-7"">
                        <div class=""d-flex justify-content-end gallery-upload"">
                            <div>
                                <i class=""fa fa-upload"" id=""gal8""></i>
                                <input type=""file"" id=""gallery8"" accept=""image/*"">
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""row mb-2"">
                    <div class=""col-4 camera-upload"">
                        <i class=""fa fa-camera"" id=""cam8""></i>
                        <input type=""file"" id=""camera8"" accept=""image/*"" capture />
                        <img id=""preview8"" src=""#"" style=""width:100%"" onerror=""this.style.display='none'"" />
                    </div>
                    <div class=""c");
            WriteLiteral("ol-8\">\r\n                        <textarea rows=\"4\" class=\"form-control\" placeholder=\"ใส่ข้อมูลรายละเอียดรูปภาพ\"></textarea>\r\n                    </div>\r\n                </div>\r\n\r\n");
            WriteLiteral(@"                <div class=""row p-2 mb-2 upload-box-header"">
                    <div class=""col-5"">
                        <div>รูปที่ 9</div>
                    </div>
                    <div class=""col-7"">
                        <div class=""d-flex justify-content-end gallery-upload"">
                            <div>
                                <i class=""fa fa-upload"" id=""gal9""></i>
                                <input type=""file"" id=""gallery9"" accept=""image/*"">
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""row mb-2"">
                    <div class=""col-4 camera-upload"">
                        <i class=""fa fa-camera"" id=""cam9""></i>
                        <input type=""file"" id=""camera9"" accept=""image/*"" capture />
                        <img id=""preview9"" src=""#"" style=""width:100%"" onerror=""this.style.display='none'"" />
                    </div>
                    <div class=""c");
            WriteLiteral("ol-8\">\r\n                        <textarea rows=\"4\" class=\"form-control\" placeholder=\"ใส่ข้อมูลรายละเอียดรูปภาพ\"></textarea>\r\n                    </div>\r\n                </div>\r\n\r\n");
            WriteLiteral(@"                <div class=""row p-2 mb-2 upload-box-header"">
                    <div class=""col-5"">
                        <div>รูปที่ 10</div>
                    </div>
                    <div class=""col-7"">
                        <div class=""d-flex justify-content-end gallery-upload"">
                            <div>
                                <i class=""fa fa-upload"" id=""gal10""></i>
                                <input type=""file"" id=""gallery3"" accept=""image/*"">
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""row mb-2"">
                    <div class=""col-4 camera-upload"">
                        <i class=""fa fa-camera"" id=""cam10""></i>
                        <input type=""file"" id=""camera10"" accept=""image/*"" capture />
                        <img id=""preview10"" src=""#"" style=""width:100%"" onerror=""this.style.display='none'"" />
                    </div>
                    <div cla");
            WriteLiteral(@"ss=""col-8"">
                        <textarea rows=""4"" class=""form-control"" placeholder=""ใส่ข้อมูลรายละเอียดรูปภาพ""></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <!--<div class=""modal fade"" id=""exampleModal"" tabindex=""-1"" role=""dialog"" aria-labelledby=""exampleModalLabel"" aria-hidden=""true"">
        <div class=""modal-dialog modal-dialog-centered"" role=""document"">
            <div class=""modal-content"">
                <div class=""modal-header"">
                    <h5 class=""modal-title"" id=""exampleModalLabel"">ประเภทเอกสารหลักฐาน</h5>
                    <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"">
                        <span aria-hidden=""true"">&times;</span>
                    </button>
                </div>
                <div class=""modal-body"">
                    <select id=""evd_type_id"" class=""form-control"" name=""evd_type_id"">
                        <option disa");
            WriteLiteral("bled>เลือกประเภทเอกสารหลักฐาน</option>-->\r\n");
            WriteLiteral(@"                    <!--</select>
                </div>
                <div class=""modal-footer"">
                    <button type=""button"" class=""btn btn-secondary"" data-dismiss=""modal"">ปิดหน้าต่าง</button>
                    <button class=""btn btn-primary"" id=""store-data-btn"">ตกลง</button>
                </div>
            </div>
        </div>
    </div>-->
</div>
");
            DefineSection("Scripts", async() => {
                WriteLiteral(@" 
<script src=""/lib/fileStyle/bootstrap-filestyle.js""></script>
<script>
  
    $(function () {
        let attachModel = JSON.parse(localStorage.getItem(""typeModel""));
        $('#attach-header').text(attachModel.typeText);

        //upload row 1 
        $(""i#cam1"").click(function () {
            let iconEl = $(this);
            let cameraEl = $(""#camera1"");
            cameraEl.trigger('click');
            cameraEl.change(function () {
                if (this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview1').attr('src', e.target.result).css(""display"", ""block"");
                    }
                    reader.readAsDataURL(this.files[0]); // convert to base64 string

                    iconEl.hide();
                }
            });
        });

        $(""i#gal1"").click(function () {
            let iconEl = $(""i#cam1"");
            let galleryEl = $(""#gal");
                WriteLiteral(@"lery1"");
            galleryEl.trigger('click');
            galleryEl.change(function () {
                if (this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview1').attr('src', e.target.result).css(""display"", ""block"");
                    }
                    reader.readAsDataURL(this.files[0]); 

                    iconEl.hide();
                }
            });
        });
        // end row 1

        //upload row 2 
        $(""i#cam2"").click(function () {
            let iconEl = $(this);
            let cameraEl = $(""#camera2"");
            cameraEl.trigger('click');
            cameraEl.change(function () {
                if (this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview2').attr('src', e.target.result).css(""display"", ""block"");
         ");
                WriteLiteral(@"           }
                    reader.readAsDataURL(this.files[0]); // convert to base64 string

                    iconEl.hide();
                }
            });
        });

        $(""i#gal2"").click(function () {
            let iconEl = $(""i#cam2"");
            let galleryEl = $(""#gallery2"");
            galleryEl.trigger('click');
            galleryEl.change(function () {
                if (this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview2').attr('src', e.target.result).css(""display"", ""block"");
                    }
                    reader.readAsDataURL(this.files[0]);

                    iconEl.hide();
                }
            });
        });
        // end row 2

        //upload row 3 
        $(""i#cam3"").click(function () {
            let iconEl = $(this);
            let cameraEl = $(""#camera3"");
            cameraEl.trigger('click');");
                WriteLiteral(@"
            cameraEl.change(function () {
                if (this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview3').attr('src', e.target.result).css(""display"", ""block"");
                    }
                    reader.readAsDataURL(this.files[0]); // convert to base64 string

                    iconEl.hide();
                }
            });
        });

        $(""i#gal3"").click(function () {
            let iconEl = $(""i#cam3"");
            let galleryEl = $(""#gallery3"");
            galleryEl.trigger('click');
            galleryEl.change(function () {
                if (this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview3').attr('src', e.target.result).css(""display"", ""block"");
                    }
                    reader.readAsDataURL(this.fil");
                WriteLiteral(@"es[0]);

                    iconEl.hide();
                }
            });
        });
        // end row 3

        //upload row 4 
        $(""i#cam4"").click(function () {
            let iconEl = $(this);
            let cameraEl = $(""#camera4"");
            cameraEl.trigger('click');
            cameraEl.change(function () {
                if (this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview4').attr('src', e.target.result).css(""display"", ""block"");
                    }
                    reader.readAsDataURL(this.files[0]); // convert to base64 string

                    iconEl.hide();
                }
            });
        });

        $(""i#gal4"").click(function () {
            let iconEl = $(""i#cam4"");
            let galleryEl = $(""#gallery4"");
            galleryEl.trigger('click');
            galleryEl.change(function () {
                if ");
                WriteLiteral(@"(this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview4').attr('src', e.target.result).css(""display"", ""block"");
                    }
                    reader.readAsDataURL(this.files[0]);

                    iconEl.hide();
                }
            });
        });
        // end row 4

        //upload row 5 
        $(""i#cam5"").click(function () {
            let iconEl = $(this);
            let cameraEl = $(""#camera5"");
            cameraEl.trigger('click');
            cameraEl.change(function () {
                if (this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview5').attr('src', e.target.result).css(""display"", ""block"");
                    }
                    reader.readAsDataURL(this.files[0]); // convert to base64 string

             ");
                WriteLiteral(@"       iconEl.hide();
                }
            });
        });

        $(""i#gal5"").click(function () {
            let iconEl = $(""i#cam5"");
            let galleryEl = $(""#gallery5"");
            galleryEl.trigger('click');
            galleryEl.change(function () {
                if (this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview5').attr('src', e.target.result).css(""display"", ""block"");
                    }
                    reader.readAsDataURL(this.files[0]);

                    iconEl.hide();
                }
            });
        });
        // end row 5

        //upload row 6 
        $(""i#cam6"").click(function () {
            let iconEl = $(this);
            let cameraEl = $(""#camera6"");
            cameraEl.trigger('click');
            cameraEl.change(function () {
                if (this.files && this.files[0]) {
                   ");
                WriteLiteral(@" let reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview6').attr('src', e.target.result).css(""display"", ""block"");
                    }
                    reader.readAsDataURL(this.files[0]); // convert to base64 string

                    iconEl.hide();
                }
            });
        });

        $(""i#gal6"").click(function () {
            let iconEl = $(""i#cam6"");
            let galleryEl = $(""#gallery6"");
            galleryEl.trigger('click');
            galleryEl.change(function () {
                if (this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview6').attr('src', e.target.result).css(""display"", ""block"");
                    }
                    reader.readAsDataURL(this.files[0]);

                    iconEl.hide();
                }
            });
        });
        // end row ");
                WriteLiteral(@"6

        //upload row 7 
        $(""i#cam7"").click(function () {
            let iconEl = $(this);
            let cameraEl = $(""#camera7"");
            cameraEl.trigger('click');
            cameraEl.change(function () {
                if (this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview7').attr('src', e.target.result).css(""display"", ""block"");
                    }
                    reader.readAsDataURL(this.files[0]); // convert to base64 string

                    iconEl.hide();
                }
            });
        });

        $(""i#gal7"").click(function () {
            let iconEl = $(""i#cam7"");
            let galleryEl = $(""#gallery7"");
            galleryEl.trigger('click');
            galleryEl.change(function () {
                if (this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onl");
                WriteLiteral(@"oad = function (e) {
                        $('#preview7').attr('src', e.target.result).css(""display"", ""block"");
                    }
                    reader.readAsDataURL(this.files[0]);

                    iconEl.hide();
                }
            });
        });
        // end row 7

        //upload row 8 
        $(""i#cam8"").click(function () {
            let iconEl = $(this);
            let cameraEl = $(""#camera8"");
            cameraEl.trigger('click');
            cameraEl.change(function () {
                if (this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview8').attr('src', e.target.result).css(""display"", ""block"");
                    }
                    reader.readAsDataURL(this.files[0]); // convert to base64 string

                    iconEl.hide();
                }
            });
        });

        $(""i#gal8"").click(function () {
");
                WriteLiteral(@"            let iconEl = $(""i#cam8"");
            let galleryEl = $(""#gallery8"");
            galleryEl.trigger('click');
            galleryEl.change(function () {
                if (this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview8').attr('src', e.target.result).css(""display"", ""block"");
                    }
                    reader.readAsDataURL(this.files[0]);

                    iconEl.hide();
                }
            });
        });
        // end row 8

        //upload row 9 
        $(""i#cam9"").click(function () {
            let iconEl = $(this);
            let cameraEl = $(""#camera9"");
            cameraEl.trigger('click');
            cameraEl.change(function () {
                if (this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        $('#pr");
                WriteLiteral(@"eview9').attr('src', e.target.result).css(""display"", ""block"");
                    }
                    reader.readAsDataURL(this.files[0]); // convert to base64 string

                    iconEl.hide();
                }
            });
        });

        $(""i#gal9"").click(function () {
            let iconEl = $(""i#cam9"");
            let galleryEl = $(""#gallery9"");
            galleryEl.trigger('click');
            galleryEl.change(function () {
                if (this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview9').attr('src', e.target.result).css(""display"", ""block"");
                    }
                    reader.readAsDataURL(this.files[0]);

                    iconEl.hide();
                }
            });
        });
        // end row 9

        //upload row 10 
        $(""i#cam10"").click(function () {
            let iconEl = $(this);
       ");
                WriteLiteral(@"     let cameraEl = $(""#camera10"");
            cameraEl.trigger('click');
            cameraEl.change(function () {
                if (this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview10').attr('src', e.target.result).css(""display"", ""block"");
                    }
                    reader.readAsDataURL(this.files[0]); // convert to base64 string

                    iconEl.hide();
                }
            });
        });

        $(""i#gal10"").click(function () {
            let iconEl = $(""i#cam10"");
            let galleryEl = $(""#gallery10"");
            galleryEl.trigger('click');
            galleryEl.change(function () {
                if (this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview10').attr('src', e.target.result).css(""display"", ""b");
                WriteLiteral("lock\");\r\n                    }\r\n                    reader.readAsDataURL(this.files[0]);\r\n\r\n                    iconEl.hide();\r\n                }\r\n            });\r\n        });\r\n        // end row 10\r\n\r\n    });\r\n</script>\r\n");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
